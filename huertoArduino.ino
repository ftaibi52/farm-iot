#include <dht.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 5

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

//Water sensor
int adc_id = 0;
int HistoryValue = 0;
char printBuffer[128];
bool regando = false;
int relay = 6;
dht DHT;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 10, 11, 12, 13);

void setup() {
  pinMode(relay, OUTPUT);
  digitalWrite(relay, HIGH);
  Serial.begin( 9600);
  sensors.begin();
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

}

void loop() {
  Serial.println("--------------");
  lcd.setCursor(0, 0);
  lcd.print("Aire ");

  int tempAmb = temAmbiente();
  lcd.print(tempAmb);
  lcd.print((char) 223); // simpbolo º
  lcd.print("C ");

  Serial.print("Temperatura ambiente: ");
  Serial.print(tempAmb);
  Serial.println("ºC");
  delay(2000); //Wait 5 seconds before accessing sensor again.
  //Fastest should be once every two seconds.

  int humAmb = humAmbiente();
  lcd.print(humAmb);
  lcd.print("%");

  if (regando) {
    lcd.print(" R");
  } else {
    lcd.print(" N");

  }

  Serial.print("Humedad ambiente: ");
  Serial.print(humAmb);
  Serial.println("%");
  delay(2000);
  // set the cursor to column 0, line 1
  // print the number of seconds since reset:
  //lcd.print(millis() / 1000);

  int luzVal = lumens();
  Serial.print("Iluminación: ");
  Serial.println(luzVal);

  int nivelAgua = waterSensor();
  Serial.print("Nivel de agua: ");
  Serial.println(nivelAgua);

  lcd.setCursor(0, 1);

  int soilTemp = soilTemperature();
  Serial.print("Temperatura en tierra: ");
  Serial.println(soilTemp);
  lcd.print("Tierra ");
  lcd.print(soilTemp);
  lcd.print((char) 223); // simpbolo º
  lcd.print("C ");

  int soilHum = soilHumidity();
  if (soilHum == 1) {

    lcd.print("Humedo");
    Serial.println("Wet");

  } else {
    Serial.println("Dry");
    lcd.print("Seco");
  }

  // Si hay agua en el tanque, es de noche y la tierra esta seca.
  if ( nivelAgua > 200 && luzVal < 30 && soilHum == 0) {
    // Parar Riego
    digitalWrite(relay, LOW);
    regando = true;
  } else {
    // Regar
    digitalWrite(relay, HIGH);
    regando = false;
  }
}

int waterSensor() {
  int value = analogRead(adc_id); // get adc value
  if (((HistoryValue >= value) && ((HistoryValue - value) > 10)) ||
      ((HistoryValue < value) && ((value - HistoryValue) > 10))) {
    //sprintf(printBuffer, "ADC%d level is %d\n", adc_id, value);
    //Serial.print(printBuffer);
    HistoryValue = value;
  }
  return value;
}

int lumens() {
  int V = analogRead(A2);
  return V;
}

int soilTemperature() {

  sensors.requestTemperatures(); // Send the command to get temperature readings
  //Serial.print("Temperature is: ");
  //Serial.print(sensors.getTempCByIndex(0)); // Why "byIndex"?
  // You can have more than one DS18B20 on the same bus.
  // 0 refers to the first IC on the wire

  int soilTemp = sensors.getTempCByIndex(0);

  return soilTemp;

}

int soilHumidity() {

  int soilMoistureValue = analogRead(A3);  //put Sensor insert into soil
  Serial.println(soilMoistureValue);

  if ( soilMoistureValue < 455 ) {
    // humedo
    return 1;
  } else {
    // seco
    return 0;
  }

}

double temAmbiente() {
  int chk = DHT.read11(A4);
  return DHT.temperature;

}
double humAmbiente() {
  int chk = DHT.read11(A4);
  return DHT.humidity;
}
